from os import system as jrny_sys


class Character:
    """Character object. Holds inventory"""
    def __init__(self, name):
        self._name = name
        self._inventory = []

    @property
    def name(self):
        """Returns the characters name"""
        return self._name

    @property
    def gender(self):
        """Returns the characters gender"""
        return self._gender

    def add_item(self, item):
        """Adds item to characters inventory"""
        if item not in self._inventory:
            self._inventory.append(item)

    def remove_item(self, item):
        """Removes item from inventory"""
        if item in self._inventory:
            self._inventory.remove(item)

    def check_item(self, item):
        """Verifies existance of item in inventory"""
        return item in self._inventory

    def __str__(self):
        output = "\n\tCharacter - - - - - - - - - \n"
        output += "\tName: {0}".format(self.name)
        output += "\tInventory:\t"
        for item in self._inventory:
            output += "[{0}] ".format(item)
        if self._inventory == []:
            output += "[EMPTY]"
        return output
