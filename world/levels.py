from time import sleep
from user import character as ch
import textwrap


class Firepit:
    """Object allows for fire attributes to be passed to parser"""
    def __init__(self):
        self._lit = False
        self._wait = False
        self._smoke = False


class Level:
    """Level Object contains """
    def __init__(self, character, scene_msg, fireplace, items, *paths):
        self._character = character
        self._items = [item for item in items]
        self._scene = textwrap.wrap(scene_msg, width=65)
        self._paths = [path for path in paths]
        self._nextpath = ""
        if fireplace is True:
            self._firepit = Firepit()
        else:
            self._firepit = None
        self._nextlvl = ""

    @property
    def nextlvl(self):
        return self._nextlvl

    @nextlvl.setter
    def nextlvl(self, target):
        self._nextlvl = target

    def print_scene(self):
        """Prints the textwrapped scene storyline"""
        for line in self._scene:
            print(line)

    def restart(self):
        """Resets the items in the start level and for user"""
        self._character._inventory.clear()
        self._items.clear()
        restore = ['helmet', 'prism']
        for item in restore:
            self._items.append(item)
