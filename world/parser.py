import cmd
from os import system as jrny_sys
import textwrap


class GameParser(cmd.Cmd):
    """Command processor for journey game."""
    def __init__(self, lvl):
        super(GameParser, self).__init__()
        self.lvl = lvl
        self.charInv = lvl._character._inventory

        # cmd characteristics
        self.doc_header = "Journey Verbs (type help <verb>):"

    def preloop(self):
        """Prints the scene storyline and corresponding messages"""
        jrny_sys('clear')
        self.lvl.print_scene()
        if 'edelweiss' in self.lvl._items:
            found = "In this hole you stumble upon Edelweiss\n"
            print(found)
        for line in self.lvl._scene:
            if line.find('spiritual journey') != -1:
                self.lvl.restart()
            elif line.find('abominable') != -1:
                if self.lvl._character.check_item('prism_pickle') == True:
                    deathtofrosty = "You threw the snow man the pickled prism"
                    deathtofrosty += " and he devoured it. Quickly, he began"
                    deathtofrosty += " to melt!"
                    frosties = textwrap.wrap(deathtofrosty, width=65)
                    for item in frosties:
                        print(item)
                    self.lvl._character._inventory.remove('prism_pickle')
                    defeatSnowman = "While the abominable snowman is melting "
                    defeatSnowman += "away, a box with a label 'The Meaning of"
                    defeatSnowman += " Life' appears from his belly"
                    colddefeat = textwrap.wrap(defeatSnowman, width=65)
                    for item in colddefeat:
                        print(item)
                    self.lvl._items.append('meaning of life')
                    self.lvl._character.remove_item('prism_pickle')
                elif self.lvl._character.check_item('pickle') == True:
                    feedingfrosty = "You threw the snow man the pickle and "
                    feedingfrosty += "he devoured it, but he is still hungry!"
                    frosties = textwrap.wrap(feedingfrosty, width=65)
                    for item in frosties:
                        print(item)
                else:
                    feedingfrosty = "You failed to calm the beast so he rips "
                    feedingfrosty += "your arms off. YOU DIE FROM DYSENTARY!"
                    frosties = textwrap.wrap(feedingfrosty, width=65)
                    for item in frosties:
                        print(item)

    def do_help(self, *args):
        """Help function that wraps the root method with a
        system clear"""
        jrny_sys('clear')
        cmd.Cmd.do_help(self, *args)

    @property
    def prompt(self):
        """Default initial prompt for command processor"""
        print("")
        if self.lvl._items != []:
            item_msg = "{0:>18}".format("Items to collect:")
            for item in self.lvl._items:
                item_msg += "\t{0}".format(item.upper())
            print(item_msg)
        if self.lvl._paths != []:
            path_msg = "{0:>18}".format("Paths to go:")
            for path in self.lvl._paths:
                path_msg += "\t{0}".format(path.upper())
            print(path_msg)
        return "\nWhat do you want to do? (help) "

    def default(self, line):
        """Default function when command entry is invalid"""
        print("The entry [{0}] is invalid.".format(line))
        return

    def do_ponder(self, item):
        """You should ponder the meaning of life"""
        if self.lvl._items != []:
            self.do_get(item)

    def do_get(self, item):
        """Acquisition of item(s). Synonymous with take()"""
        item = item.lower()
        user_items = item.split()

        if item == 'meaning of life':
            final_statement = "Congratulations on finding the meaning of"
            final_statement += " life. Your journey is completed"
            finals = textwrap.wrap(final_statement, width=65)
            for item in finals:
                print(item)

            input("\n<THE END>")
            quit()
        if self.lvl._items == []:
            print("There are no items to collect")
        elif item == 'all':
            for item in self.lvl._items:
                self.lvl._character.add_item(item)
            self.lvl._items.clear()
        elif item in self.lvl._items:
            self.lvl._items.remove(item)
            self.lvl._character.add_item(item)
        else:
            print("Item [{0}] not found".format(item))

    def do_take(self, item):
        """Acquisition of item(s). Synonymous with get()"""
        self.do_get(item)

    def do_collect(self, item):
        """Acquisition of item(s). Synonymous with get()"""
        self.do_get(item)

    def do_drop(self, item):
        """Drop item from inventory. If you drop item it is lost forever"""
        if self.charInv is []:
            print("You have nothing to drop")
        elif item.lower() in self.charInv:
            print("You dropped {0}".format(item))
            self.lvl._character.remove_item(item)
            self.lvl._items.append(item.lower())
        elif item.lower() == 'all':
            print("You dropped all your items")
            for item in self.charInv:
                self.lvl._items.append(item)
            self.charInv.clear()
        else:
            print("You cannot drop {0}".format(item))

    def do_open(self, path):
        """Open entity in scene"""
        if path in self.lvl._paths:
            self.lvl._nextlvl = path
            return True
        else:
            print("Action [{0}] cannot be performed".format(path))

    def do_exit(self, path):
        """Use to exit a level"""
        path_words = path.split()
        if path_words == []:
            print("What do you want to exit?")
        elif 'cave' in path_words:
            self.lvl._nextlvl = 'exit cave'
            return True
        else:
            print("You cannot exit to {0}".format(path))

    def do_home(self, line):
        """Go home you failure"""
        path = 'home'
        if path in self.lvl._paths:
            self.lvl._nextlvl = path
            return True
        else:
            print("Cannot go {0}".format(path))

    def do_start(self, line):
        """Go back to the outside of your house"""
        path = 'start'
        if path in self.lvl._paths:
            self.lvl._nextlvl = path
            return True
        else:
            print("Cannot go {0}".format(path))

    def do_north(self, line):
        """Go North to the Snowy Mountain Tops"""
        path = 'north'
        if path in self.lvl._paths:
            self.lvl._nextlvl = path
            return True
        else:
            print("Cannot go {0}".format(path))

    def do_west(self, line):
        """Go West to the Thundering Oceans"""
        path = 'west'
        if path in self.lvl._paths:
            self.lvl._nextlvl = path
            return True
        else:
            print("Cannot go {0}".format(path))

    def do_south(self, line):
        """Go South to the Valley of Short things"""
        path = 'south'
        if path in self.lvl._paths:
            self.lvl._nextlvl = path
            return True
        else:
            print("Cannot go {0}".format(path))

    def do_east(self, line):
        """Go East to the Dark Caverns"""
        path = 'east'
        if path in self.lvl._paths:
            self.lvl._nextlvl = path
            return True
        else:
            print("Cannot go {0}".format(path))

    def do_up(self, line):
        """Go Up"""
        path = 'up'
        if path in self.lvl._paths:
            self.lvl._nextlvl = path
            return True
        else:
            print("Cannot go {0}".format(path))

    def do_down(self, line):
        """Go Down"""
        path = 'down'
        if path in self.lvl._paths:
            self.lvl._nextlvl = path
            return True
        else:
            print("Cannot go {0}".format(path))

    def do_enter(self, path):
        """Enter passageway"""
        if path in self.lvl._paths:
            self.lvl._nextlvl = path
            return True
        else:
            print("Cannot enter {0}".format(path))

    def do_examine(self, item):
        """Examine an item"""
        item = item.lower()
        if item == 'door':
            print("Looks like a door can be opened or closed.")
        if item == 'pickle':
            print("The pickel has a prism shaped hole in it")
        if item == 'prism':
            print("It looks like this will kill the snowman")
        if item == 'helmet':
            print("Wow, that looks like the same shape as a statue opening")
        if item == 'edelweiss':
            print("This plant will make alot of smoke if burned")
        if item == 'fire' or item == 'pit':
            if self.lvl._firepit._lit is False:
                print("Looks like a good place to burn something if lit")
            else:
                print("Edelweiss would make alot of smoke in this fire")
        if item == 'meaning of life':
            print("You may want to ponder that")
        if item == 'statue' and self.lvl._firepit is not None:
            if self.lvl._firepit._smoke is True:
                print("The statues doorway is open")
            else:
                statue_print = "The statue has a helmet shaped doorway"
                " that only opens from smoke"
                statues_prnt = textwrap.wrap(statue_print, width=65)
                for item in statues_prnt:
                    print(item)
        elif item == 'statue':
            statue_print = "The statue has a helmet shaped doorway"
            " that only opens from smoke"
            statues_prnt = textwrap.wrap(statue_print, width=65)
            for item in statues_prnt:
                print(item)

    def do_light(self, item):
        """Light something on fire"""
        if self.lvl._firepit is not None:
            if item == "pit":
                self.lvl._firepit._lit = True
                print("You lit the fire pit")
            elif item == "fire":
                self.lvl._firepit._lit = True
                print("You lit the fire pit")
        else:
            failed_light = "You tried to light {0}, but the wind"
            " blew it out"
            print(failed_light)

    def do_wait(self, line):
        """Allows you to Wait for a while"""
        if self.lvl._firepit is not None:
            self.lvl._firepit._wait = True
            print("The fire looks hot enough to burn something in")
        else:
            print("What are you waiting for?")

    def do_quit(self, line):
        """Quit the Game"""
        quit()

    def do_q(self, line):
        """Quit the Game"""
        quit()

    def do_eof(self, line):
        """Quit the Game"""
        quit()

    def do_show(self, line):
        """Show user straps"""
        jrny_sys('clear')
        print(self.lvl._character)

    def do_put(self, variables):
        """Allows you to put items into things"""
        items = variables.split()

        if 'pickle' in items and 'pickle' in self.charInv:
            if 'prism' in items and 'prism' in self.charInv:
                print("You put the prism in the pickle!")
                self.charInv.append('prism_pickle')
                self.lvl._character.remove_item('pickle')
                self.lvl._character.remove_item('prism')
        elif self.lvl._firepit is not None:
            if self.lvl._firepit._lit is True:
                if 'edelweiss' in items and 'fire' in items:
                    if self.lvl._firepit._wait is True:
                        self.lvl._firepit._smoke = True
                        burning = "You put the Edelweiss in the fire, It is "
                        burning += "making a lot of smoke."
                        burns = textwrap.wrap(burning, width=65)
                        for item in burns:
                            print(item)
                    else:
                        burning = "The fire is not hot enough."
                        burning += " You need to wait."
                        burns = textwrap.wrap(burning, width=65)
                        for item in burns:
                            print(item)
                    self.lvl._character.remove_item('edelweiss')

            if self.lvl._firepit._smoke is True and 'statue' in items:
                if 'helmet' in items:
                    print("You put the helmet in the Statue!")
                    print("A hollowed pickle fell out. Now you have a pickle!")
                    self.lvl._character.remove_item('helmet')
                    self.charInv.append('pickle')
            elif 'statue' in items:
                print("The door is closed")
        else:
            print("Uh, that didn't work")

    def do_cave(self, line):
        """You should enter cave"""
        pass

    def do_firepit(self, line):
        """You should light fire pit"""
        pass

    def do_pit(self, line):
        """You should light fire pit"""
        pass

    def do_fire_pit(self, line):
        """You should light fire pit"""
        pass

    def do_door(self, line):
        """You should open the door"""
        pass
