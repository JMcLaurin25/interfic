import scenes as scn
from sys import exit
from world import levels as lvl
from world import parser as prs


class Engine:
    """The Engine class runs the map levels in a continuous loop,
    while receiving and setting the next levels begin() function"""
    def __init__(self, level):
        self._level = level

    def parse_level(self, level):
        """This function runs the parser with the correct
        variables"""
        prs.GameParser(level).cmdloop()

    def play(self):
        """ function for the game loop"""
        cur_level = self._level.begin_game()

        while True:
            self.parse_level(cur_level)
            next_lvl_name = cur_level.nextlvl
            if next_lvl_name is None:
                exit(1)
            cur_level = self._level.next_level(next_lvl_name)


class Map:
    """The Map class stores the character, starting level, scenes
    and the dictionary of levels"""
    def __init__(self, character, level):
        self._plyr = character
        self._level = level
        msg = scn.Scenes()
        outside_door = lvl.Level(
            self._plyr,
            msg.scene02,
            False,
            [],
            'north',
            'south',
            'east',
            'west'
        )
        self._levels = {
            'door': outside_door,
            'start': outside_door,
            'home': lvl.Level(
                self._plyr,
                msg.scene01,
                False,
                ['helmet', 'prism'],
                'door'
            ),
            'north': lvl.Level(
                self._plyr,
                msg.north,
                False,
                [],
                'home'
            ),
            'south': lvl.Level(
                self._plyr,
                msg.south,
                False,
                [],
                'start'
            ),
            'east': lvl.Level(
                self._plyr,
                msg.east,
                False,
                ['edelweiss'],
                'up'
            ),
            'west': lvl.Level(
                self._plyr,
                msg.west,
                False,
                [],
                'home'
            ),
            'cave': lvl.Level(
                self._plyr,
                msg.cave,
                True,
                [],
                'exit cave'
            ),
            'up': lvl.Level(
                self._plyr,
                msg.caveEntry,
                False,
                [],
                'cave'
            ),
            'exit cave': lvl.Level(
                self._plyr,
                msg.caveExit,
                False,
                [],
                'north',
                'south',
                'west'
            )
        }

    def next_level(self, level_name):
        """Retrieves the game key from the levels dictionary"""
        return self._levels.get(level_name, 'Level not found')

    def begin_game(self):
        """Initations the game loop with the beginning level"""
        return self.next_level(self._level)

    def __str__(self):
        output = "\tself._plyr: {0}\n".format(self._plyr.name)
        output += "\tself._level: {0}\n".format(self._levelStart)
        return output
