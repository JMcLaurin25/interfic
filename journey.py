#!/usr/bin/python3

from os import system as jrny_sys
from os import getlogin as getguy
from user import character as ch
from world import game_engine as ge


def main():
    """Beginning function for journey. Creates the character, the map
    and runs the engine"""
    jrny_sys('clear')
    try:

        # getguy() from import os
        name = getguy()
        print("Hello {0}, Welcome to this wonderful world.".format(name))
    except:
        print("Welcome to this wonderful world.")

        name = input("What's your name? ")
        name = name.title()

    explorer = ch.Character(name)

    print(explorer)
    input("...")

    game_map = ge.Map(explorer, 'home')
    game = ge.Engine(game_map)
    game.play()

if __name__ == "__main__":
    main()
