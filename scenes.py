
class Scenes:
    """Contains the storyline of the game loop"""
    __instance = None

    def __new__(cls):
        """Makes Scenes class a 'singleton'"""
        if Scenes.__instance is None:
            Scenes.__instance = object.__new__(cls)
        return Scenes.__instance

    def __init__(self):

        # Opening Scene
        self.scene01 = "You are on a spiritual journey to find the meaning"
        self.scene01 += " of life. Your mentor has tasked you with finding "
        self.scene01 += "the mystical caverns of enlightenment, and "
        self.scene01 += "overcoming the challenges required to find the "
        self.scene01 += "ultimate answer."
        self.scene01 += "\nTo begin your journey a collection of"
        self.scene01 += " items have been placed before you"

        # OPEN DOOR
        self.scene02 = "Which direction will you go now? "
        self.scene02 += "East, to the dark caverns? West, to the thundery "
        self.scene02 += "oceans? North to the Snowy mountain tops? or South"
        self.scene02 += " to the valley of short things?"

        self.north = "You have arrived in the snowy mount tops where an "
        self.north += "abominable snow man finds you. The only way to defeat"
        self.north += " him to get him to swallow the mystic prism, but he "
        self.north += "only eats humans and pickels."

        self.south = "You have arrived in the valley, where everything is "
        self.south += "too small for you. The people are kind but there is "
        self.south += "nothing for you."

        # EAST
        self.east = "On your journey to the dark caverns you slip and fall "
        self.east += "into a hole at the bottom of a cave opening."
        self.east += "\nNow that you're in a cold wet hole. The only way "
        self.east += "out is to climb up a rope ladder."

        self.west = "You arrived at the Thundery Oceans and don't know how "
        self.west += "to swim. What were you thinking!? YOU DIE FROM "
        self.west += "DYSENTARY!"

        # UP
        self.caveEntry = "At the top of the ladder is a wide dark cave opening"

        # ENTER CAVE
        self.cave = "Now that you have entered the cave you discover an old "
        self.cave += "fire pit and a statue with an odd closed 'helmet-shaped'"
        self.cave += " doorway."

        # EXIT CAVE
        self.caveExit = "You are now outside the cave and have clear "
        self.caveExit += "passageways."
